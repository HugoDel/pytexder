import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pytexder",
    version="0.0.3",
    author="Hugo Delannoy",
    author_email="delannoy_45@msn.com",
    description="A very simple template engine using latex file",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/HugoDel/pytexder",
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)


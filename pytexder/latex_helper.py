import logging
import os
import subprocess

from pytexder.reader import get_full_path


class LatexHelper:

    def __init__(self, working_dir, input_file, output_file):
        self.working_dir = working_dir
        self.output_file = output_file
        self.input_file = input_file
        self.__test_latext_working()

    def render(self):
        out = subprocess.run(['lualatex', '-output-format=pdf', '-interaction=nonstopmode',
                              f'-output-directory={self.working_dir}',
                              f'-jobname={self.output_file}', get_full_path(self.working_dir, self.input_file)],
                             stdout=subprocess.PIPE)
        if out.returncode == 0:
            logging.info(f'{get_full_path(self.working_dir, self.output_file)} generated from {self.input_file}')
        else:
            logging.error(out.stdout.decode())
        self.__cleanup()

    def __cleanup(self):
        os.remove(get_full_path(self.working_dir, f'{self.output_file}.aux'))
        os.remove(get_full_path(self.working_dir, f'{self.output_file}.log'))

    @staticmethod
    def __test_latext_working():
        out = subprocess.run(['pdflatex', '-version'], stdout=subprocess.PIPE)
        if out.returncode != 0:
            raise ImproperlyConfigured('PDFLatex seems to not be installed...')
        else:
            logging.info(out.stdout.decode())

    @staticmethod
    def escape(s):
        s = s.replace('_', '\_')
        s = s.replace('$', '\$')
        return s


class ImproperlyConfigured(Exception):
    pass

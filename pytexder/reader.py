import logging
import os
import re
from builtins import staticmethod, IOError, ValueError


class IOHelper:
    extension = 'pytexder'
    data = None

    def __init__(self, file_name, directory='/', data=None):
        self.data = data
        print(self.data)
        self.file_name = self.extension_checker(file_name, self.extension)
        if os.path.isdir(directory):
            self.directory = directory
        else:
            raise IOError(f'{directory} is not a directory')

    @staticmethod
    def extension_checker(file_name, expt_ext):
        grp = re.split('\.', file_name)
        if grp[len(grp)-1] == expt_ext or (grp[len(grp)-2] == expt_ext and grp[len(grp)-1] == 'tmp'):
            return file_name
        else:
            raise ValueError(f'{file_name} should have "{expt_ext}" extension')

    def read_file(self):
        if not self.data:
            with open(os.path.join(self.directory, self.file_name), 'r') as f:
                self.data = f.read()
        return self.data

    def get_full_path(self):
        return os.path.join(self.directory, self.file_name)

    def write_file(self):
        nline = None
        if self.data:
            logging.info(f'Trying to open {self.get_full_path()}')
            with open(self.get_full_path(), 'w') as f:
                nline = f.write(self.data)
                logging.info(f'Written {nline} lines to {self.get_full_path()}')
        else:
            logging.warning('No data to write')
        return nline


def get_full_path(dire, file):
    return os.path.join(dire, file)


def remove_extension(file):
    file = file[::-1]
    grp = re.split('\.', file, 1)
    return grp[1][::-1]

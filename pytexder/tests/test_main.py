import hashlib
import os
import unittest
from pytexder import PyTexder


class MainCaseTest(unittest.TestCase):

    def test_variable_replacement(self):
        dir = os.getcwd() + '/pytexder/tests/test_files'
        ptr = PyTexder(dir)
        ptr.render('fullexemple.pytexder', {'title': 'Mon Titre', 'articles': ['Premier article', 'Deuxieme article'],
                                            'texts': ['Ceci est un premier texte', 'Ceci est un deuxième texte'],
                                            'unused': 'Useless'},
                   'fullexemple')
        hasher = hashlib.md5()
        with open(dir + '/fullexemple_ref.pdf', 'rb') as f:
            buf = f.read()
            hasher.update(buf)
        ref = hasher.hexdigest()
        hasher = hashlib.md5()
        with open(dir + '/fullexemple_ref.pdf', 'rb') as f:
            buf = f.read()
            hasher.update(buf)
        assert ref == hasher.hexdigest()

import os
from builtins import ValueError

from pytexder.reader import IOHelper
import unittest


class MainCaseTest(unittest.TestCase):

    def test_extension_validator(self):
        good_file = 'exemple.pytexder'
        bad_file1 = 'exemple'
        bad_file2 = 'exemple.py'
        assert IOHelper.extension_checker(good_file, 'pytexder') == good_file
        with self.assertRaises(ValueError):
            IOHelper.extension_checker(bad_file1, 'pytexder')
        with self.assertRaises(ValueError):
            IOHelper.extension_checker(bad_file2, 'pytexder')

    def test_directory_validation(self):
        with self.assertRaises(IOError):
            IOHelper('exemple.pytexder', './nonexistingfoldertesting')

    def test_read_file(self):
        io = IOHelper('reading.pytexder', os.getcwd() + '/pytexder/tests/test_files')
        assert "This is a test file reading" == io.read_file()
        io.data = "Data to write"
        io.write_file()
        assert "Data to write" == io.read_file()
        # Let's retrieve the original content
        io.data = "This is a test file reading"
        io.write_file()

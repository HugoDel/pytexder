import os

from pytexder import PyTexder

ptr = PyTexder(os.getcwd() + '/pytexder/tests/test_files')
ptr.render('fullexemple.pytexder', {'title': 'Mon Titre', 'articles': ['Premier article', 'Deuxieme article'],
                                    'texts': ['Ceci est un premier texte', 'Ceci est un deuxième texte'],
                                    'unused': 'Useless'},
           'fullexemple')